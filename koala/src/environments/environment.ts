// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'kola-1acbe',
    appId: '1:801237761665:web:2f25f66af6d1a55e4f5780',
    databaseURL: 'https://kola-1acbe-default-rtdb.firebaseio.com',
    storageBucket: 'kola-1acbe.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyDc8jFPqmG2s84LefPr_cdAAPa8FA0taPI',
    authDomain: 'kola-1acbe.firebaseapp.com',
    messagingSenderId: '801237761665',
    measurementId: 'G-0MC9D6Z75C',
  },
  production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
