import { Component, OnInit,Input} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  coord = {x:0 , y:0}; 
  paint = false;
  canvas:any;
  ctx :any;
  ligne = [
    [0, 0]
   ];

  title = 'koala';
  

  ngOnInit(): void {
    throw new Error('Method not implemented.');
    /** */
    this.canvas = <HTMLCanvasElement> document.getElementById('canvas');
    this.ctx = this.canvas.getContext('2d');
    
    window.addEventListener('load', ()=>{ 
      this.resize(); // Resizes the canvas once the window loads
      document.addEventListener('mousedown', this.startPainting);
      document.addEventListener('mouseup', this.stopPainting);
      document.addEventListener('mousemove', this.sketch);
      window.addEventListener('resize', this.resize);
  });
  
  }

   resize(this: any){
    this.ctx.canvas.width = window.innerWidth;
    this.ctx.canvas.height = window.innerHeight;
  }

   getPosition(event: { clientX: number; clientY: number; }){
    this.coord.x = event.clientX - this.canvas.offsetLeft;
    this.coord.y = event.clientY - this.canvas.offsetTop;

    this.ligne.push([this.coord.x,this.coord.y])
  }

   startPainting(event: { clientX: number; clientY: number; }){
    this.paint = true;
    this.getPosition(event);
  }

   stopPainting(){
    this.paint = false;
  }

   sketch(event: any){
    if (!this.paint) return;
    if(this.ctx==null)return;
    this.ctx.beginPath();
      
    this.ctx.lineWidth = 5;
    this.ctx.lineCap = 'round';
      
    this.ctx.strokeStyle = 'green';
    this.ctx.moveTo(this.coord.x, this.coord.y);
    this.getPosition(event);
    this.ctx.lineTo(this.coord.x , this.coord.y);
    this.ctx.stroke();
  }

  SendData(data:string){
     
  
    }


    
  
}
/**
 * 
 *  var canvas = <HTMLCanvasElement> document.getElementById('canvas');
      if(canvas!=null){
        if (canvas.getContext) {
          var ctx = canvas.getContext('2d');
          if(ctx!=null){
            ctx.fillStyle = "#D74022";
            ctx.fillRect(25, 25, 150, 150);
      
            ctx.fillStyle = "rgba(0,0,0,0.5)";
            ctx.clearRect(60, 60, 120, 120);
            ctx.strokeRect(90, 90, 80, 80);
            console.log("this.MaPenserBete",data);
          }

        }

 */