import { Component, OnDestroy, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-stiky',
  templateUrl: './stiky.component.html',
  styleUrls: ['./stiky.component.scss']
})
export class StikyComponent implements OnInit,OnDestroy  {
  interval: number|undefined;

  constructor() { }
  ngOnDestroy(): void {
    throw new Error('Method not implemented.');
  }

  ngOnInit(): void {
  }

  close(){
    clearInterval(this.interval);
  }
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(event.previousIndex, event.currentIndex);
  }

}
