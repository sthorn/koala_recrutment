import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StikyComponent } from './stiky/stiky.component';

import { environment } from '../environments/environment';
import { DirectiveDirective } from './directive/directive.directive';

@NgModule({
  declarations: [
    AppComponent,
    StikyComponent,
    DirectiveDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
