# Koala_recrutment

Test technique Koala


## Getting started

For run this code you need to have this angular version and library
            _                      _                 ____ _     ___
            / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
        / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
        / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
        /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
                        |___/
            

        Angular CLI: 13.0.0
        Node: 16.13.0
        Package Manager: npm 8.1.0
        OS: linux x64

        Angular: 13.0.0
        ... animations, cli, common, compiler, compiler-cli, core, forms
        ... platform-browser, platform-browser-dynamic, router

        Package                         Version
        ---------------------------------------------------------
        @angular-devkit/architect       0.1300.0
        @angular-devkit/build-angular   13.0.0
        @angular-devkit/core            13.0.0
        @angular-devkit/schematics      13.0.0
        @angular/fire                   0.0.0
        @schematics/angular             13.0.0
        rxjs                            7.4.0
        typescript                      4.4.4

if your OS is Linux run:
```bash
  npm uninstall -g @angular/cli
  npx @angular/cli@13 update @angular/core@13 @angular/cli@13

```
To run code you need just to run this command:
- ```ng serve```
and open this address [http://localhost:4200/](http://localhost:4200/) on your computer.

